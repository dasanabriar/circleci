package com.co.bogota.app.service;

import com.co.bogota.app.models.entity.Client;

import java.util.List;

public interface IClientService {
    List<Client> findAll();
    Client findById(long id);
    Client save(Client client);
    boolean delete(Client client);
    Client update(Client client);
}
