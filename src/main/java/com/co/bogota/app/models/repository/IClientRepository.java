package com.co.bogota.app.models.repository;

import com.co.bogota.app.models.entity.Client;
import org.springframework.data.repository.CrudRepository;

public interface IClientRepository extends CrudRepository<Client,Long> {
}
