package com.co.bogota.app.controller;

import com.co.bogota.app.models.entity.Client;
import com.co.bogota.app.service.IClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class ClientController {

    @Autowired
    private IClientService clientService;
    
    @GetMapping(value="/clients")
    public List<Client> listClients(){
        return clientService.findAll();
    }

    @GetMapping("/clients/{id}")
    public Client show(@PathVariable long id) {
        return this.clientService.findById(id);
    }

    @PostMapping("/clients")
    @ResponseStatus(HttpStatus.CREATED)
    public Client create(@RequestBody Client client) {
        client.setCreateDate(new Date());
        this.clientService.save(client);
        return client;
    }

    @PutMapping("/clients/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Client update(@RequestBody Client client, @PathVariable long id) {
        client.setId(id);
        return clientService.update(client);
    }

    @DeleteMapping("/clients/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        Client foundClient = this.clientService.findById(id);
        this.clientService.delete(foundClient);
    }
}
